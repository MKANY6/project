Install
=======

`BenchNIRS` is available on `PyPI <https://pypi.org/project/benchnirs/>`_ and
can be installed via :code:`pip`:

.. code-block:: console

   $ pip install benchnirs
