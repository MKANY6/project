.. BenchNIRS documentation master file

Welcome to BenchNIRS's documentation!
=====================================

`BenchNIRS` is a benchmarking framework for machine learning with fNIRS.

Downloading the framework: https://gitlab.com/HanBnrd/benchnirs

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   modules
   example


.. |uncheck| raw:: html

    <input type="checkbox">


Recommendation checklist
========================

Below is a proposed checklist of recommendations towards best practice for machine learning classification with fNIRS (for brain-computer interface applications).

| **Methodology**:
| |uncheck| Plan classes before designing the experiment (to avoid using return to baseline as control baseline task)  
| |uncheck| Use nested cross-validation, also called double cross-validation with the outer cross-validation (leaving out the test sets) for evaluation and the inner cross-validation (leaving out the validation sets) for the optimisation of models
| |uncheck| Optimise the hyperparameters (with grid-search for instance) on validation sets
| |uncheck| Use the test sets for evaluation and nothing else (no optimisation should be performed with the test set)
| |uncheck| Create the training, validation and test sets in accordance with what the model is hypothesised to generalise (eg. unseen subject, unseen session, etc.), thanks to group k-fold cross-validation for example
| |uncheck| Pay attention to not include test data when performing normalisation
| |uncheck| Take extra care to not have any of the sets overlap (training, validation and test sets), the test set used to report results more than anything must consist of unseen data only
| |uncheck| Pay attention to class imbalance (using metrics more appropriate than accuracy such as F1 score for example)
| |uncheck| Perform a statistical analysis to find significance of the results when comparing results to chance level and classifiers to each other

| **Reporting**:
| |uncheck| Describe what data is used as input of the classifier and its shape
| |uncheck| Describe the number of input examples in the dataset
| |uncheck| Describe the details of the cross-validations implementations
| |uncheck| Describe the details of each model used including the architecture of the model and every hyperparameter
| |uncheck| Describe which hyperparameters have been optimised and how
| |uncheck| Clearly state the number of classes and the chance level
| |uncheck| Provide all necessary information related to the statistical analysis of the results, including the name of the tests, the verification of their assumptions and the p-values


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
