import itertools
import numpy as np
import pandas as pd
from scipy import stats
from statsmodels.stats.multitest import multipletests


DATASETS = {'herff_2014_nb': ['1-back', '2-back', '3-back'],
            'shin_2018_nb': ['0-back', '2-back', '3-back'],
            'shin_2018_wg': ['baseline', 'word generation'],
            'shin_2016_ma': ['baseline', 'mental arithmetic'],
            'bak_2019_me': ['right', 'left', 'foot']}
CONFIDENCE = 0.05  # stat confidence at 95 %


results_folder = '../results/generalised_<date>'
models = ['LDA', 'SVC', 'kNN', 'ANN', 'CNN', 'LSTM']
# models = ['LDA', 'SVC', 'kNN', 'ANN']
# results_folder = '../results/sliding_window_<date>'
combinations = list(itertools.combinations(models, 2))

# Stats
print('Stats...')
with open(f'{results_folder}/stats.md', 'w') as w:
    df = pd.read_csv(f'{results_folder}/results.csv', delimiter=';')
    w.write('## Comparison of model accuracies to chance level\n\n')
    w.write('|Dataset|Model|Shapiro p-value|Test|Statistic|p-value|\n')
    w.write('|:---:|:---:|:---:|:---:|:---:|:---:|\n')
    anova_table = ''
    for dataset in DATASETS.keys():
        print(f"-----\n{dataset}")
        dataset_accuracies = []
        chance_level = 1 / len(DATASETS[dataset])
        normality = True
        for model in models:
            w.write(f'|{dataset}|{model}|')
            sub_df = df[(df['dataset'] == dataset) & (df['model'] == model)]
            accuracies = sub_df['accuracy'].to_numpy()
            dataset_accuracies.append(accuracies)
            # Check normality of the distribution
            _, p_shap = stats.shapiro(accuracies)
            w.write(f'{p_shap}|')
            if p_shap > CONFIDENCE:
                # t-test
                s_tt, p_tt = stats.ttest_1samp(accuracies, chance_level,
                                               alternative='greater')
                w.write(f't-test|{s_tt}|{p_tt}|\n')
            else:
                normality = False
                # Wilcoxon
                s_wilcox, p_wilcox = stats.wilcoxon(accuracies-chance_level,
                                                    alternative='greater')
                w.write(f'Wilcoxon|{s_wilcox}|{p_wilcox}|\n')
        _, p_bart = stats.bartlett(*dataset_accuracies)
        if normality and (p_bart > CONFIDENCE):
            s_anova, p_anova = stats.f_oneway(*dataset_accuracies)
            anova_table += f'|{dataset}|{p_bart}|ANOVA|{s_anova}|{p_anova}|\n'
            if p_anova <= CONFIDENCE:
                # Run pairwise ttests
                pairs = []
                p_values = []
                for pair in combinations:
                    m1, m2 = pair
                    df1 = df[(df['dataset'] == dataset) & (df['model'] == m1)]
                    a1 = df1['accuracy'].to_numpy()
                    df2 = df[(df['dataset'] == dataset) & (df['model'] == m2)]
                    a2 = df2['accuracy'].to_numpy()
                    if np.mean(a1) > np.mean(a2):
                        _, p_pair = stats.ttest_rel(a1, a2,
                                                    alternative='greater')
                    elif np.mean(a1) < np.mean(a2):
                        _, p_pair = stats.ttest_rel(a2, a1,
                                                    alternative='greater')
                        pair = m2, m1
                    else:
                        raise RuntimeError('Average accuracies are the same')
                    pairs.append(pair)
                    p_values.append(p_pair)
                reject, c_p_values, _, _ = multipletests(p_values,
                                                         method='bonferroni')
                for i, pair in enumerate(pairs):
                    print(f"{pair} | {reject[i]} | {c_p_values[i]}")
        else:
            s_kru, p_kru = stats.kruskal(*dataset_accuracies)
            anova_table += f'|{dataset}|{p_bart}|Kruskal|{s_kru}|{p_kru}|\n'
            if p_kru <= CONFIDENCE:
                # Run pairwise ttests
                pairs = []
                p_values = []
                for pair in combinations:
                    m1, m2 = pair
                    df1 = df[(df['dataset'] == dataset) & (df['model'] == m1)]
                    a1 = df1['accuracy'].to_numpy()
                    df2 = df[(df['dataset'] == dataset) & (df['model'] == m2)]
                    a2 = df2['accuracy'].to_numpy()
                    if np.mean(a1) > np.mean(a2):
                        _, p_pair = stats.ttest_rel(a1, a2,
                                                    alternative='greater')
                    elif np.mean(a1) < np.mean(a2):
                        _, p_pair = stats.ttest_rel(a2, a1,
                                                    alternative='greater')
                        pair = m2, m1
                    else:
                        raise RuntimeError('Average accuracies are the same')
                    pairs.append(pair)
                    p_values.append(p_pair)
                reject, c_p_values, _, _ = multipletests(p_values,
                                                         method='bonferroni')
                for i, pair in enumerate(pairs):
                    print(f"{pair} | {reject[i]} | {c_p_values[i]}")
    w.write('\n\n## Comparison of model accuracies to each other\n\n')
    w.write('|Dataset|Bartlett p-value|Test|Statistic|p-value|\n')
    w.write(f'|:---:|:---:|:---:|:---:|:---:|\n{anova_table}')
